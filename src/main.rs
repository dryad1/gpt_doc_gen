use std::error::Error;

use std::fs;
use std::io;
use std::path::Path;
use std::process::{Command};

use async_openai::types::{
    AssistantTools,
    AssistantToolsCode,
    AssistantToolsRetrieval,
    CreateFileRequest,
};
use async_openai::{
    types::{
        CreateAssistantRequestArgs,
        CreateMessageRequestArgs,
        CreateRunRequestArgs,
        CreateThreadRequestArgs,
        MessageContent,
        RunStatus,
    },
    Client,
};
use clap::Parser;
use quote::{ToTokens};
use regex::Regex;


use syn::{
    File,
    Item,
    ItemEnum,
    ItemFn,
    ItemStruct,
};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args
{
    /// Path to the source code to document
    #[arg(short, long)]
    file_path: String,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>>
{
    let args = Args::parse();
    let mut file_content = load_file(&args.file_path.to_string());

    // Chatgpt Init
    // --------------------------------------------------------------------------------
    let query = [("limit", "1")]; //limit the list responses to 1 message

    //create a client
    let client = Client::new();

    //create a thread for the conversation
    let thread_request = CreateThreadRequestArgs::default().build()?;
    let thread = client.threads().create(thread_request.clone()).await?;

    let client = Client::new();
    let request = CreateFileRequest {
        file: args.file_path.clone().into(),
        purpose: "assistants".into(),
    };
    let response = client.files().create(request).await?;
    let file_id = response.id;

    //get user input
    let assistant_name = "Documentation Assistant".to_string();

    let instructions =
        "Help to create documentation blocks for rust code. These should be concise but descriptive. Pay attention to the grammar and use of articles. When you reply to a prompt please crate only the comment text. Don't add any actual code int he response."
            .to_string();

    //create the assistant
    let assistant_request = CreateAssistantRequestArgs::default()
        .name(&assistant_name)
        .instructions(&instructions)
        .model("gpt-3.5-turbo-0125")
        .tools(vec![
            AssistantTools::Retrieval(AssistantToolsRetrieval {
                r#type: "retrieval".to_string(),
            }),
            AssistantTools::Code(AssistantToolsCode {
                r#type: "code_interpreter".to_string(),
            }),
            // AssistantTools::Function(AssistantToolsFunction {
            //     r#type: "function".to_string(),
            //     function: FunctionObject {
            //         name: "update_file".to_string(),
            //         description: Some(
            //             "Changes the contents of a rust file with
            // comments.".to_string(),         ),
            //         parameters: Some(Value::from(
            //             "\
            //              'parameters': {
            //                 'comment_contents': {
            //                         'type': 'string',
            //                         'description': 'Name of the school.'
            //                     },
            //                 }",Guard
            //         )),
            //     },
            // }),
        ])
        .file_ids(vec![file_id.clone()])
        .build()?;
    let assistant = client.assistants().create(assistant_request).await?;
    //get the id of the assistant
    let assistant_id = &assistant.id;
    // ---------------------------------------------------------------------------------------------

    let mut file: File =
        syn::parse_str(file_content.as_str()).expect("Failed to parse Rust code");

    let item_count = file.items.len();
    for i in 0..item_count
    {
        let item = &mut file.items[i];
        let is_valid = match item
        {
            Item::Struct(_) | Item::Enum(_) | Item::Fn(_) => true,
            _ => false,
        };
        if !is_valid
        {
            continue;
        }
        if has_doc_comment(item)
        {
            continue;
        }

        let input = format!(
"Can you please make a descriptive documentation block for {}. Please show only the documentation comments, \
don't say things like 'certainly' or 'This is' when introducing it. Please don't say anything other \
than the documentation. Don't add the function signature below the comment, add just the comment.",
            get_item_name(item).unwrap());

        // ChatGpt query -----
        //create a message for the thread
        let message = CreateMessageRequestArgs::default()
            .role("user")
            .content(input.clone())
            .build()?;

        //attach message to the thread
        let _message_obj = client
            .threads()
            .messages(&thread.id)
            .create(message)
            .await?;

        //create a run for the thread
        let run_request = CreateRunRequestArgs::default()
            .assistant_id(assistant_id)
            .build()?;
        let run = client
            .threads()
            .runs(&thread.id)
            .create(run_request)
            .await?;

        //wait for the run to complete
        let mut comment_content = String::default();
        let mut awaiting_response = true;
        while awaiting_response
        {
            //retrieve the run
            let run = client.threads().runs(&thread.id).retrieve(&run.id).await?;
            //check the status of the run
            match run.status
            {
                RunStatus::Completed =>
                {
                    awaiting_response = false;
                    // once the run is completed we
                    // get the response from the run
                    // which will be the first message
                    // in the thread

                    //retrieve the response from the run
                    let response =
                        client.threads().messages(&thread.id).list(&query).await?;
                    //get the message id from the response
                    let message_id = response.data.get(0).unwrap().id.clone();
                    //get the message from the response
                    let message = client
                        .threads()
                        .messages(&thread.id)
                        .retrieve(&message_id)
                        .await?;
                    //get the content from the message
                    let content = message.content.get(0).unwrap();
                    //get the text from the content
                    let text = match content
                    {
                        MessageContent::Text(text) => text.text.value.clone(),
                        MessageContent::ImageFile(_) =>
                        {
                            panic!("images are not supported in the terminal")
                        }
                    };
                    comment_content = text;
                }

                _ =>
                {}
            }
            //wait for 1 second before checking the status again
            std::thread::sleep(std::time::Duration::from_secs(5));
        }

        // -------------------

        add_comment(item, &mut file_content, &comment_content);
    }

    client.assistants().delete(assistant_id).await?;
    client.threads().delete(&thread.id).await?;
    client.files().delete(&file_id).await?;

    write_and_fmt("tmp.rs", file_content).expect("Could not export file.");
    // Print the modified syntax tree
    // modify_file(&mut file);

    Ok(())
}

fn write_and_fmt<P: AsRef<Path>, S: ToString>(path: P, code: S) -> io::Result<()>
{
    fs::write(&path, code.to_string())?;

    Command::new("rustfmt").arg(path.as_ref()).spawn()?.wait()?;

    Ok(())
}

fn get_item_name(item: &Item) -> Option<String>
{
    match item
    {
        Item::Struct(ItemStruct { ident, .. }) | Item::Enum(ItemEnum { ident, .. }) =>
        {
            Some(ident.to_string())
        }
        Item::Fn(ItemFn { sig, .. }) => Some(sig.ident.to_string()),
        _ => None,
    }
}

fn has_doc_comment(item: &Item) -> bool
{
    match item
    {
        Item::Struct(s) =>
        {
            // Check if the struct has a doc comment
            let has_doc_comment = s.attrs.iter().any(|attr| attr.path().is_ident("doc"));

            has_doc_comment
        }
        Item::Fn(s) =>
        {
            // Check if the struct has a doc comment
            let has_doc_comment = s.attrs.iter().any(|attr| attr.path().is_ident("doc"));

            has_doc_comment
        }
        Item::Enum(s) =>
        {
            // Check if the struct has a doc comment
            let has_doc_comment = s.attrs.iter().any(|attr| attr.path().is_ident("doc"));

            has_doc_comment
        }
        _ =>
        {
            // For other item types, you can add similar logic
            false
        }
    }
}

fn load_file(path: &str) -> String
{
    match fs::read_to_string(path)
    {
        Ok(content) =>
        {
            // Successfully read the file
            content
        }
        Err(err) =>
        {
            // Handle the error
            panic!("Error reading the file: {}", err);
        }
    }
}

fn add_comment(item: &mut Item, file: &mut String, comment: &str)
{
    let comment_text = format!("{}", comment);
    let mut comment_text = comment_text
        .replace("\\n", " ")
        .replace("```rust", "")
        .replace("```", "");
    comment_text = "/// [AI Generated]\n".to_owned() + &comment_text.trim_start();

    let _pattern = {
        let pattern = item
            .to_token_stream()
            .to_string()
            .replace("*", "\\*")
            .replace(" ", "\\s*")
            .replace("\t", "\\s*")
            .replace("\n", "\\s*")
            .replace("{", "\\{")
            .replace("}", "\\}")
            .replace("[", "\\[")
            .replace("]", "\\]")
            .replace("(", "\\(")
            .replace(")", "\\)")
            .replace("&", "\\&")
            .replace("_", "\\_")
            .replace("#", "\\#");

        let re = Regex::new(pattern.as_str()).expect("Failed to create regex");
        if let Some(captures) = re.captures(file)
        {
            // Access the first capture group
            if let Some(first_match) = captures.get(0)
            {
                // Extract the matched substring
                let matched_text = first_match.as_str();
                let comment_and_text = comment_text + matched_text;
                *file = file.replace(matched_text, comment_and_text.as_str());
            }
        }
        else
        {
            println!("Pattern not found");
        }
    };
}
